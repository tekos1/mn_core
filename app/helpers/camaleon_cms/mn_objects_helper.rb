module CamaleonCms::MnObjectsHelper

	def standard_types
		["String","Text","Date","DateTime","Integer","Number","Select","Checkbox","Image"]
	end

	def attribute_value_input_field attr, val=nil
		case attr.o_type
		when "Image"
			file_field_tag "mn_attribute_value[#{attr.name}]" , class: "form-control col-10" 
		when "Select"
			select_tag "mn_attribute_value[#{attr.name}]", options_for_select(attr.mn_attribute_options.pluck(:name),val), class: "form-control col-10"
		when "Text"
		 	text_area_tag "mn_attribute_value[#{attr.name}]", val, placeholder: attr.name , class: "form-control col-10" 
		else		
		 	text_field_tag "mn_attribute_value[#{attr.name}]", val, placeholder: attr.name , class: "form-control col-10" 
		end
	end

	#attribute value
	def show_value av 
		case av.mn_attribute.o_type 
		when "Image"
			image_tag av.image.url, class: "img-fluid", style: "max-width: 200px" if av.image.url
		else
			av.value
		end
	end

	def process_attr val 
		#last check point (for extention)
		if val 
			if UtilService.is_image? val
				image_tag val , style: "max-width: 200px"
			else
				val
			end
		end
	end

	def list_show_types
		["List","Show", "Insert"]
	end


	# return objects [id, name]
	# change to custom post types
	def object_names
		MnObject.pluck(:id, :name)
	end

	def attribute_names obj_id
		MnObject.find(obj_id).mn_attributes.pluck(:name)
	end

	def post_column_names post_type
		# attrs in custom fields by post types
		CamaleonCms::Post.column_names.select { |c| ["title","status","user_id"].include? c }
	end

	def attr_included_in post_type, key
		attrs = post_column_names post_type
		attrs.include? key.to_s.gsub(/.*\./,'')
	end

end