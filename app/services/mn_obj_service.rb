class MnObjService

	class << self

		def create label, params
			label.gsub!(" ","")
			params = get_params(params)
			res = Neo4j::Node.create(params, label)
			# {id: res.id,labels: res.labels, props: res.props}
		end

		def find_obj obj_id
			Neo4j::Node.load(obj_id, NEO_SESSION)
		end

		def find obj_id
			res = find_obj obj_id
			{labels: res.labels, props: res.props}
		end


		def list obj, options={}
			# query to match object
			label = obj.name.gsub(" ","");
			res = Neo4j::Core::Query.new.match(n: label).return(n: CamaleonCms::Post.column_names)
			# res.pluck ( attributes )
			res = map_list(res)
			res
		end

		def map_list res
			res.each_with_index.map do |s,index|
			   v = s.to_h  
			   v[:id] = res.pluck(:n)[index].neo_id
			   v  
 			end 
 		end

		def create_rel source_id, dest_id, name
			Neo4j::Relationship.create name, find_obj(source_id), find_obj(dest_id)
		end


		def update neo_id, params 
			params = get_params(params)
			obj = find_obj(neo_id)
			obj.update_props(params)
		end



		#delete object (remove file from bucket)
		def delete neo_id
			obj= find_obj(neo_id)
			obj.props.each do |k,v|
				if UtilService.is_image? v
					delete_image v 
				end
			end
			obj.del
		end



		def get_attr_value obj_id, attr_name
			if obj_id
				find_obj(obj_id).props[attr_name.to_sym]
			end
			# rescue attr not found
		end


		private

		def delete_image url
			reg_key = /\/[^\/]+\/[^\/]+\..+$/
			key = url.match(reg_key).to_s[1..-1]
			s3 = Aws::S3::Client.new
			s3.delete_object(bucket: BUCKET_NAME, key: key)
		end

		# transform and remove non essential params
		def get_params params
			#connect to s3 and upload image , then add image url
			params = params.to_h
			
			params.each do |key,value|
				if value.class == ActionDispatch::Http::UploadedFile

			    s3 = Aws::S3::Resource.new
					obj = s3.bucket(BUCKET_NAME).object("mn_object/#{Time.now.to_i}#{value.original_filename}")
					obj.upload_file(value.path, {acl: 'public-read'})
					params[key] = obj.public_url
					
				end
			end

	    #add execption .. 

		end

	end


end
